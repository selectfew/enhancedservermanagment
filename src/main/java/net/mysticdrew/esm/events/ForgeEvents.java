package net.mysticdrew.esm.events;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.NameFormat;
import net.mysticdrew.esm.utils.ChatUtils;
import net.mysticdrew.esm.utils.LogHelper;
import net.mysticdrew.esm.utils.user.PlayerUtils;
import net.mysticdrew.esm.utils.user.UserEventManager;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent;

public class ForgeEvents {

	@SubscribeEvent
	public void playerDeathEvent(LivingDeathEvent event) {
		if (event.entity instanceof EntityPlayerMP) {
			EntityPlayerMP player = (EntityPlayerMP) event.entity;
			UserEventManager.instance.playerDied(player);
		}

	}
	
	@SubscribeEvent
	public void playerChatEvent(ServerChatEvent event) {
		event.player.refreshDisplayName();
	}
	
	@SubscribeEvent
	public void playerNameEvent(NameFormat event) {
		try
		{
			event.displayname = ChatUtils.instance.chatEvent(event.username);
		}
		catch (NullPointerException e)
		{
			/* This is not the best way to deal with the problem, however it has successfully resolved it */
			event.displayname = "UNKNOWN";
		}
	}
	
	@SubscribeEvent
	public void commandEvent(CommandEvent event) {
		for (String player : PlayerUtils.instance.getOnlinePlayers()) {
			PlayerUtils.instance.getPlayerEntityByName(player).refreshDisplayName();
		}
	}
}
