/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.configuration;

import java.io.File;

import net.minecraft.util.StatCollector;
import net.minecraftforge.common.config.Configuration;
import net.mysticdrew.esm.reference.Reference;
import net.mysticdrew.esm.utils.LogHelper;
import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class ConfigHandler {
	
	public static Configuration config;
	public static int adminLevel;
	public static String path;
	
	public static void init(File configFile) {
		
		if (config == null) {
			config =  new Configuration(configFile);
			loadConfigs();
			LogHelper.info("loading configs");
		}
	
	}
	
	@SubscribeEvent
	public void onConfigChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event) {
		if (event.modID.equalsIgnoreCase(Reference.MOD_ID)) {
			loadConfigs();
		}
	}
	
	private static void loadConfigs() {
		try 
		{
			config.load();
			adminLevel = config.getInt(StatCollector.translateToLocal("config.admin.adminlevel"), "admin", 4, 1, 4, StatCollector.translateToLocal("config.admin.description"));
			path = config.getString(StatCollector.translateToLocal("config.path.configpath"), "paths", "./esm/", StatCollector.translateToLocal("config.path.description"));
						
		} catch (Exception e) {
			LogHelper.warn("No Config file found");
		} finally {
			if (config.hasChanged()) {
				config.save();
			}
		}
	}
}
