/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.commands.vanilla;

import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.dedicated.DedicatedServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.StatCollector;

import com.mojang.authlib.GameProfile;

/**
 * This is a copy of the vanilla whitelist command with the on|off switches removed.
 * @author Mysticdrew
 *
 */
public class Whitelist extends CommandBase implements ICommand {
	
	@Override
	public String getCommandName() {
		return "whitelist";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return StatCollector.translateToLocal("command.esm.whitelist.usage");
	}
	
	

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		String userName = sender.getCommandSenderName();
		if (args.length >= 1) {
			MinecraftServer server = DedicatedServer.getServer();

			if (args[0].equals("list")) {
				sender.addChatMessage(new ChatComponentTranslation(
						"commands.whitelist.list", new Object[] {
								Integer.valueOf(server.getConfigurationManager().func_152598_l().length),
								Integer.valueOf(server.getConfigurationManager().getAvailablePlayerDat().length) }));
				String[] astring1 = server.getConfigurationManager().func_152598_l();
				sender.addChatMessage(new ChatComponentText(joinNiceString(astring1)));
				return;
			}
			GameProfile gameprofile;

			if (args[0].equals("add")) {
				if (args.length < 2) {
					throw new WrongUsageException("commands.whitelist.add.usage", new Object[0]);
				}

				gameprofile = server.func_152358_ax().func_152655_a(args[1]);

				if (gameprofile == null) {
					throw new CommandException("commands.whitelist.add.failed",new Object[] { args[1] });
				}

				server.getConfigurationManager().func_152601_d(gameprofile);
				func_152373_a(sender, this,"commands.whitelist.add.success",new Object[] { args[1] });
				server.getConfigurationManager().loadWhiteList();
				func_152373_a(sender, this,"commands.whitelist.reloaded", new Object[0]);
				return;
			}

			if (args[0].equals("remove")) {
				if (args.length < 2) {
					throw new WrongUsageException("commands.whitelist.remove.usage", new Object[0]);
				}

				gameprofile = server.getConfigurationManager().func_152599_k().func_152706_a(args[1]);

				if (gameprofile == null) {
					throw new CommandException("commands.whitelist.remove.failed",new Object[] { args[1] });
				}

				server.getConfigurationManager().func_152597_c(gameprofile);
				func_152373_a(sender, this,"commands.whitelist.remove.success",new Object[] { args[1] });
				server.getConfigurationManager().loadWhiteList();
				func_152373_a(sender, this,"commands.whitelist.reloaded", new Object[0]);
				return;
			}
		}
		throw new WrongUsageException(StatCollector.translateToLocal("command.esm.whitelist.usage"), new Object[0]);

	}
	
	 public List addTabCompletionOptions(ICommandSender sender, String[] args)
	    {
	        if (args.length == 1)
	        {
	            /**
	             * Returns a List of strings (chosen from the given strings) which the last word in the given string array
	             * is a beginning-match for. (Tab completion).
	             */
	            return getListOfStringsMatchingLastWord(args, new String[] {"list", "add", "remove", "reload"});
	        }
	        else
	        {
	            if (args.length == 2)
	            {
	                if (args[0].equals("remove"))
	                {
	                    /**
	                     * Returns a List of strings (chosen from the given strings) which the last word in the given string
	                     * array is a beginning-match for. (Tab completion).
	                     */
	                    return getListOfStringsMatchingLastWord(args, MinecraftServer.getServer().getConfigurationManager().func_152598_l());
	                }

	                if (args[0].equals("add"))
	                {
	                    /**
	                     * Returns a List of strings (chosen from the given strings) which the last word in the given string
	                     * array is a beginning-match for. (Tab completion).
	                     */
	                    return getListOfStringsMatchingLastWord(args, MinecraftServer.getServer().func_152358_ax().func_152654_a());
	                }
	            }

	            return null;
	        }
	    }


}
