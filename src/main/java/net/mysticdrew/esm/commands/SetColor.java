package net.mysticdrew.esm.commands;


import java.util.List;
import java.util.UUID;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.ChatUtils;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.user.PlayerUtils;


public class SetColor extends CommandBase implements ICommand {
	private static final String COMMAND_NAME = "setcolor";
	private static final String COMMAND_USAGE =  StatCollector.translateToLocal("command.color.usage") + ChatUtils.instance.getColorListWithColor();
	
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return COMMAND_USAGE;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if (args.length == 1) {
			UUID uuid = PlayerUtils.instance.getUserByName(sender.getCommandSenderName()).getId();
			User user = EnhancedServerModeration.instance.userMap.get(uuid);
			String colorValue = args[0].toLowerCase();
			EnumChatFormatting color = ChatUtils.instance.getColorMap().get(colorValue);
			
			user.setNameColor(color);
			
			ChatUtils.instance.sendChat(sender, "Your username has been set to " + color + args[0]);
			
			if (PlayerUtils.instance.getOnlinePlayers().contains(user.getName())) {
				EnhancedServerModeration.userMap.put(user.getId(), user);
			}
			FileUtils.instance.saveUser(user);
			((EntityPlayerMP) sender).refreshDisplayName();
		} else {
			ChatUtils.instance.sendChat(sender, COMMAND_USAGE);
		}
	}
	
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}
	
	public List addTabCompletionOptions(ICommandSender sender, String[] args) {

		if (args.length >= 0) {
			return getListOfStringsMatchingLastWord(args, ChatUtils.instance.getColorList());
		}

		return null;
	}

}
