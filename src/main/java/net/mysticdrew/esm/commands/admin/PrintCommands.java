/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.commands.admin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.StatCollector;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.configuration.ConfigHandler;
import net.mysticdrew.esm.model.Command;
import net.mysticdrew.esm.utils.ChatUtils;
import net.mysticdrew.esm.utils.FileHelper;

public class PrintCommands extends AdminCommandBase {
	private static final String COMMAND_NAME = "printcommands";
	private static final String COMMAND_USAGE =  StatCollector.translateToLocal("command.print-commands.usage");
	private static final String ID_TEXT = StatCollector.translateToLocal("command.print-commands.id");
	private static final String PRINTED = StatCollector.translateToLocal("command.print-commands.printed");
	private static final String DESCRIPTION_TEXT = StatCollector.translateToLocal("command.print-commands.description");
	private static final String OWNER_TEXT = StatCollector.translateToLocal("command.print-commands.owner");
	private static final String COMMAND_OUTPUT_FILE = "commands.html";
	
	
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return COMMAND_USAGE;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		EnhancedServerModeration esm = EnhancedServerModeration.instance;
		List<Command> commands = sort(new ArrayList<Command>(esm.registeredCommands.values()));
		StringBuffer sb = new StringBuffer();
		sb.append("<html><body>");
		sb.append("<table border=\"1\" cellpadding=\"5\" ><tr><th>");
		sb.append(OWNER_TEXT + "</th><th>" + ID_TEXT + "</th><th>" + DESCRIPTION_TEXT + "</th>");
		sb.append("</tr>");
		for (Command s : commands) {
			String usage = nullCheck(StatCollector.translateToLocal(esm.registeredCommands.get(s.getCommandName()).getCommandUsage()));
			String owner = esm.registeredCommands.get(s.getCommandName()).getCommandOwner();
			usage = usage.replace("<", "&lt");
			usage = usage.replace(">", "&gt");
			sb.append("<tr>");
			sb.append("<td>" + owner + "</td><td>" + s.getCommandName() + "</td><td>" + nullCheck(usage) + "</td>");
			sb.append("</tr>");
		}
		sb.append("</table></body></html>");
		
		FileHelper.writeFile(ConfigHandler.path, COMMAND_OUTPUT_FILE, sb.toString());
		ChatUtils.instance.sendChat(sender, "ESM Commands Printed");
	}
	
	private String nullCheck(String str) {
		if (str == null) {
			return "unknown";
		} else {
			return str;
		}
	}
	
	private ArrayList<Command> sort(ArrayList<Command> list) {
		Collections.sort(list, new Comparator<Command>() {
		    public int compare(Command v1, Command v2) {
		        return v1.getCommandOwner().compareTo(v2.getCommandOwner());
		    }
		});
		return list;
	}

}
