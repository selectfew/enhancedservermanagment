package net.mysticdrew.esm.commands.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.StatCollector;
import net.mysticdrew.esm.model.Group;
import net.mysticdrew.esm.utils.ChatUtils;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.GroupUtils;
import net.mysticdrew.esm.utils.ServerUtils;

public class EditGroup extends AdminCommandBase {
	private static final String COMMAND_NAME = "editgroup";
	private static final String COMMAND_USAGE =  StatCollector.translateToLocal("command.edit-group.usage");
	private static final String NO_GROUP = StatCollector.translateToLocal("general.user.notexist");
	private static final String CREATED = StatCollector.translateToLocal("command.edit-group.created");
	private static final String ADDED_TO = StatCollector.translateToLocal("command.edit-group.addedto");
	private static final String REMOVED_FROM = StatCollector.translateToLocal("command.remove-user.removed");
	private static final String GROUP_DELETE = StatCollector.translateToLocal("command.edit-group.groupdel");
	private static final String CONTAIN_CMD = StatCollector.translateToLocal("command.edit-group.containcmd");
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return COMMAND_USAGE;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if (args.length >= 2) {
			Group group = FileUtils.instance.loadGroup(args[0]);
			
			if (args[1].equals("add") && args.length > 2) {
				addGroup(sender, group, args);
			} else if (args[1].equals("remove") && args.length > 2) {
				removeGroup(sender, group, args);
			} else if (args[1].equals("delete")) {
				deleteGroup(sender, group, args);
			} else if (args[1].equals("list")) {
				listGroup(sender, group, args);
			}else {
				ChatUtils.instance.sendChat(sender, COMMAND_USAGE);
			} 
			
		} else {
			ChatUtils.instance.sendChat(sender, COMMAND_USAGE);
		}
	}
	

	
	private void addGroup(ICommandSender sender, Group group, String[] args) {
		Set<String> commands;
		List<String> cmdText = new ArrayList<String>();
		if(group != null) {
			commands = new HashSet<String>(group.getCommandsList());
		} else {
			group = new Group();
			group.setName(args[0]);
			ServerUtils.instance.addGroupToWorld(group.getName());
			commands = new HashSet<String>();
			ChatUtils.instance.sendChat(sender, args[0] + CREATED);
		}
		for (int i = 2; i < args.length; i++) {
			commands.add(args[i]);
			cmdText.add(args[i]);
		}
		ChatUtils.instance.sendChat(sender, cmdText + ADDED_TO + args[0]);
		GroupUtils.instance.addGroupToMap(group);
		group.setCommandsList(new ArrayList(commands));
		FileUtils.instance.saveGroup(group);
	}
	
	private void removeGroup(ICommandSender sender, Group group, String[] args) {
		Set<String> commands;
		List<String> cmdText = new ArrayList<String>();
		if (group != null) {
			commands = new HashSet<String>(group.getCommandsList());
			for (int i = 2; i < args.length; i++) {
				commands.remove(args[i]);
				cmdText.add(args[i]);
			}
			ChatUtils.instance.sendChat(sender, cmdText + REMOVED_FROM + args[0]);
			GroupUtils.instance.addGroupToMap(group); 
			group.setCommandsList(new ArrayList(commands));
			FileUtils.instance.saveGroup(group);
		} else {
			ChatUtils.instance.sendChat(sender,args[0] + NO_GROUP);
		}
	}
	
	private void deleteGroup(ICommandSender sender, Group group, String[] args) {
		if (group != null) {
			ChatUtils.instance.sendChat(sender, GROUP_DELETE + group.getName());
			FileUtils.instance.deleteGroup(group.getName());
		} else {
			ChatUtils.instance.sendChat(sender, args[0] + NO_GROUP);
		}
	}
	
	private void listGroup(ICommandSender sender, Group group, String[] args) {
		if (group != null) {
			List<String> commands = group.getCommandsList();
			ChatUtils.instance.sendChat(sender, args[0] + CONTAIN_CMD + commands);
		} else {
			ChatUtils.instance.sendChat(sender, args[0] + NO_GROUP);
		}
	}
	
	public List addTabCompletionOptions(ICommandSender sender, String[] args) {
		if (args.length == 1) {
			/**
			 * Returns a List of strings (chosen from the given strings) which
			 * the last word in the given string array is a beginning-match for.
			 * (Tab completion).
			 */
			return getListOfStringsMatchingLastWord(args,
					ServerUtils.instance.getWorldGroups());
		} else {
			if (args.length == 2)
	        {
	            /**
	             * Returns a List of strings (chosen from the given strings) which the last word in the given string array
	             * is a beginning-match for. (Tab completion).
	             */
	            return getListOfStringsMatchingLastWord(args, new String[] {"list", "add", "remove", "delete"});
	        }
			
			if (args.length > 2 && !args[1].equalsIgnoreCase("list") ) {

				/**
				 * Returns a List of strings (chosen from the given strings)
				 * which the last word in the given string array is a
				 * beginning-match for. (Tab completion).
				 */
				return getListOfStringsMatchingLastWord(args,
						ServerUtils.instance.getCommandList());
			}

			return null;
		}
	}
}
