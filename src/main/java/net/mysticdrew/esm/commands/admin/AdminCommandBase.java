/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.commands.admin;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.configuration.ConfigHandler;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.user.PlayerUtils;

public abstract class AdminCommandBase extends CommandBase {
	
	public AdminCommandBase(){
		EnhancedServerModeration.adminCommands.add(getCommandName());
	}

	@Override
	public abstract String getCommandName();

	@Override
	public abstract String getCommandUsage(ICommandSender sender);

	@Override
	public abstract void processCommand(ICommandSender sender, String[] args);
	
	
	@Override
	public int getRequiredPermissionLevel() {
		return ConfigHandler.adminLevel;
	}
		
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		EntityPlayerMP mpPlayer;
		if (sender instanceof EntityPlayerMP) {
			mpPlayer = (EntityPlayerMP)sender;
			//dev user
//			if (mpPlayer.getCommandSenderName().equalsIgnoreCase("mysticdrew")) {
//				return true;
//			}
		}
		return super.canCommandSenderUseCommand(sender);
	}
	
	protected void saveUser(User user) {
		if (PlayerUtils.instance.getOnlinePlayers().contains(user.getName())) {
			EnhancedServerModeration.userMap.put(user.getId(), user);
		}
		FileUtils.instance.saveUser(user);
	}
}
