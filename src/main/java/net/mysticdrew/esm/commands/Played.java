package net.mysticdrew.esm.commands;

import java.util.Date;

import com.mojang.authlib.GameProfile;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.StatCollector;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.utils.ChatUtils;
import net.mysticdrew.esm.utils.FileUtils;
import net.mysticdrew.esm.utils.user.PlayerUtils;

public class Played extends CommandBase implements ICommand {
	private static final String COMMAND_NAME = "played";
	private static final String COMMAND_USAGE =  StatCollector.translateToLocal("command.played.usage");
	private static final String YOU_SESSION = StatCollector.translateToLocal("command.played.yousession");
	private static final String YOU_TOTAL = StatCollector.translateToLocal("command.played.youtotal");
	private static final String YOU_DIED = StatCollector.translateToLocal("command.played.youdied");
	private static final String TIMES = StatCollector.translateToLocal("general.times.text");
	
	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return COMMAND_USAGE;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		PlayerUtils pu = PlayerUtils.instance;
		GameProfile gp = pu.instance.getUserByName(sender.getCommandSenderName());
		User user = FileUtils.instance.loadUser(gp.getId());
		Long millis = pu.instance.calculateTimePlayed(user, new Date());
		String playedText = pu.instance.timePlayed(millis);
		Long deaths = (user.getDeaths() != null ? user.getDeaths() : 0);
		ChatUtils.instance.sendChat(sender, YOU_SESSION + pu.instance.getDateDifference(user.getLoginDate(), new Date()));
		ChatUtils.instance.sendChat(sender, YOU_TOTAL + playedText);
		ChatUtils.instance.sendChat(sender, YOU_DIED + deaths + TIMES);
	}
	
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}
}
