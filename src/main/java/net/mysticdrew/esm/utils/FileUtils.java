/*	EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.mysticdrew.esm.utils;

import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.mysticdrew.esm.configuration.ConfigHandler;
import net.mysticdrew.esm.model.World;
import net.mysticdrew.esm.model.Group;
import net.mysticdrew.esm.model.User;

public class FileUtils {
	private static String USER_PATH = ConfigHandler.path + "users/";
	private static String GROUP_PATH = ConfigHandler.path + "groups/";
	
	public static FileUtils instance = new FileUtils();
	
	public User loadUser(UUID uuid) {
		Gson gson = new Gson();
		return gson.fromJson(FileHelper.readFile(USER_PATH,  uuid + ".json"), User.class);
	}
	
	public boolean saveUser(User user) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return FileHelper.writeFile(USER_PATH, user.getId() + ".json", gson.toJson(user));
	}
	
	public boolean deleteUser(UUID id) {
		return FileHelper.deleteFile(USER_PATH, id + ".json");
	}
	
	public Group loadGroup(String groupName) {
		Gson gson = new Gson();
		return gson.fromJson(FileHelper.readFile(GROUP_PATH,  groupName + ".json"), Group.class);
	}
	
	public boolean saveGroup(Group group) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return FileHelper.writeFile(GROUP_PATH, group.getName() + ".json", gson.toJson(group));
	}
	
	public boolean deleteGroup(String groupName) {
		return FileHelper.deleteFile(GROUP_PATH, groupName + ".json");
	}
	
	public World loadWorldFile(String path) {
		Gson gson = new Gson();
		return gson.fromJson(FileHelper.readFile(path,  "esmworld.json"), World.class);
	}
	
	public boolean saveWorldFile(String path, World world) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return FileHelper.writeFile(path, "esmworld.json", gson.toJson(world));
	}
}
