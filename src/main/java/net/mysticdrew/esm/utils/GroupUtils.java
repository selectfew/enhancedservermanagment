package net.mysticdrew.esm.utils;


import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.model.User;
import net.mysticdrew.esm.model.Group;


public class GroupUtils {
	public static GroupUtils instance = new GroupUtils();
	
	public void addGroupToMap(Group group) {
		EnhancedServerModeration.groupMap.put(group.getName(), group);
	}
	
	public boolean validateUserGroupCommand(UUID userKey, String command) {
		Group group;
		Set<String> groupCommandList;
		User user =  EnhancedServerModeration.instance.userMap.get(userKey);
		for (String s : user.getGroupList()) {
			group = EnhancedServerModeration.instance.groupMap.get(s);
			groupCommandList = new HashSet<String>(group.getCommandsList());
			if (groupCommandList.contains(command)){
				return true;
			}
			
		}
		return false;
	}
}
