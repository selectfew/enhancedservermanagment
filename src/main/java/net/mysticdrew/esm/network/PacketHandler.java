package net.mysticdrew.esm.network;


import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;

public class PacketHandler {
	
	//public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(Reference.MOD_ID.toLowerCase());
	public static final SimpleNetworkWrapper MAP_CHANNEL = NetworkRegistry.INSTANCE.newSimpleChannel("world_info");
	
	public static void init() {			
			MAP_CHANNEL.registerMessage(WorldIdMessage.WorldIdListener.class, WorldIdMessage.class, 0, Side.SERVER);
	}
}

