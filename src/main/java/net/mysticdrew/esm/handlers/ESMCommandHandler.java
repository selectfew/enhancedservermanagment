package net.mysticdrew.esm.handlers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.minecraft.command.CommandException;
import net.minecraft.command.CommandHandler;
import net.minecraft.command.CommandNotFoundException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.network.rcon.RConConsoleSource;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.CommandEvent;
import net.mysticdrew.esm.EnhancedServerModeration;
import net.mysticdrew.esm.core.ESMCommandCore;
import net.mysticdrew.esm.model.Command;
import net.mysticdrew.esm.utils.LogHelper;
import cpw.mods.fml.common.Loader;

public class ESMCommandHandler extends CommandHandler {
	
	
	@Override
	public int executeCommand(ICommandSender sender, String commandString)
    {
		commandString = commandString.trim();

        if (commandString.startsWith("/"))
        {
        	commandString = commandString.substring(1);
        }

        String[] commandStringList = commandString.split(" ");
        String commandName = commandStringList[0];     
        
        String[] commandArgs = new String[commandStringList.length - 1];

        for (int i = 1; i < commandStringList.length; ++i)
        {
            commandArgs[i - 1] = commandStringList[i];
        }
                
        ICommand command = (ICommand)super.getCommands().get(commandName);

        int j = 0;
        ChatComponentTranslation chatcomponenttranslation;

        try
        {
            if (command == null)
            {
                throw new CommandNotFoundException();
            }
            
            if (command.canCommandSenderUseCommand(sender) || ESMCommandCore.canPlayerUseCommand(sender, command.getCommandName()))
            {
            	CommandEvent event = new CommandEvent(command, sender, commandArgs);
				if (MinecraftForge.EVENT_BUS.post(event)) {
					if (event.exception != null) {
						throw event.exception;
					}
					return 1;
				}
				
				command.processCommand(sender, commandArgs);
				++j;

			}
            else
            {
                ChatComponentTranslation chatcomponenttranslation2 = new ChatComponentTranslation("commands.generic.permission", new Object[0]);
                chatcomponenttranslation2.getChatStyle().setColor(EnumChatFormatting.RED);
                sender.addChatMessage(chatcomponenttranslation2);
            }
        }
        catch (WrongUsageException wrongusageexception)
        {
            chatcomponenttranslation = new ChatComponentTranslation("commands.generic.usage", new Object[] {new ChatComponentTranslation(wrongusageexception.getMessage(), wrongusageexception.getErrorOjbects())});
            chatcomponenttranslation.getChatStyle().setColor(EnumChatFormatting.RED);
            sender.addChatMessage(chatcomponenttranslation);
        }
        catch (CommandException commandexception2)
        {
            chatcomponenttranslation = new ChatComponentTranslation(commandexception2.getMessage(), commandexception2.getErrorOjbects());
            chatcomponenttranslation.getChatStyle().setColor(EnumChatFormatting.RED);
            sender.addChatMessage(chatcomponenttranslation);
        }
        catch (Throwable throwable)
        {
            chatcomponenttranslation = new ChatComponentTranslation("commands.generic.exception", new Object[0]);
            chatcomponenttranslation.getChatStyle().setColor(EnumChatFormatting.RED);
            sender.addChatMessage(chatcomponenttranslation);
            LogHelper.error("Couldn\'t process command: \'" + commandString + "\'" + throwable);
        }

        return j;
    }
	
	@Override
    public ICommand registerCommand(ICommand command)
    {
        List list = command.getCommandAliases();
        super.getCommands().put(command.getCommandName(), command);
        
        ICommandSender sender = new RConConsoleSource();
        
        Command commandModel = new Command();
        
        String modName;
        try {
        	modName =  Loader.instance().activeModContainer().getModId();
        } catch (NullPointerException e) {
        	modName = "Minecraft";
        }
        
        try {
        	commandModel.setCommandName(command.getCommandName());
        	commandModel.setCommandUsage(command.getCommandUsage(sender));
        	commandModel.setCommandOwner(modName);
        } catch (Exception e) {
        	commandModel.setCommandUsage("<--Mod Developer FAIL!");
        }

        EnhancedServerModeration.instance.registeredCommands.put(command.getCommandName(), commandModel);
        
        LogHelper.info("Registering command: " + command.getCommandName() + " Mod: " + modName);
        
        if (list != null)
        {
            Iterator iterator = list.iterator();

            while (iterator.hasNext())
            {
                String s = (String)iterator.next();
                ICommand icommand1 = (ICommand)super.getCommands().get(s);

                if (icommand1 == null || !icommand1.getCommandName().equals(s))
                {
                	super.getCommands().put(s, command);
                }
            }
        }

        return command;
    }
	

	@Override
	public List getPossibleCommands(ICommandSender sender) {
        List<ICommand> commandList = new ArrayList<ICommand>();
        Set<ICommand> command = new HashSet<ICommand>(super.getCommands().values());
        
        for (ICommand c : command) {
        	commandList.add(c);
        }
        return commandList;
	}

}
