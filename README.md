Enhanced Server Moderation - 0.9.8
=========

Create server moderators without giving the player admin rights. 

This is a server side mod for Minecraft, with it you can assign commands to players. 

  - Simple administrator commands to add and remove users.
  - Create moderation groups with preset commands and assign a user to a group.
  - A simple API for other mod developers so they can have Enhanced Server Moderation handle access to their commands.
  - New Commands being added all the time.
  - ####Creates Unique WorldID for use with JourneyMap 5.0+

####Join #esm on irc.esper.net

    Admin tools and commands:

     - /adduser <username> <commands>
        - add as many commands as needed, separated by a space.
        - example: "/adduser mysticdrew whitelist kick ban"
    
     
     - /removeuser <username> commands
        - same as adduser just removes the commands from the user.
    
    
     - /printcommands
        - creates a commands.txt in the esm directory with a list of all command and a small description.
    
    
    - /playerinfo <username>
        - displays a bunch of info about the user. 
        
        
    - Group Commands
        - /assigngroup <user> <groups>
            - assings groups to a user
        - /removegroup <user> <groups>
            - removes groups from a user
        - /editgroup <group> <add|remove|delete|list> <commands>
            - add: adds the commands to a group, creates the group if it doenst exist.
            - remove: removes commands from the group.
            - delete: deletes the group.
            - list: lists the commands in the group.


Version
----

0.7.6

Links
-----------

* [Twitter] - mysticdrew's twitter
* [MinecraftForum] - Mod on MCF
* [Curse] - Mod on curse
* [FTB] - FTB Forums

Installation
--------------
 * Built with forge version: 10.13.0.1208
 * Put EnhancedServerModeration.x.x.x.jar into the server's mod directory
 * This mod is not needed on the client side, generally discouraged to put on the client.

 
Mod Packs!!!!!!
==========
 * EnhancedServerModeration is not allowed in client modpacks. NO NOT ASK.
 * This is because it is a server side mod. 
 * This mod is intended for server administrators. 
 * If you wish, you can include a like for server admins that use your mod pack so they can manually install it. 
 
Exceptions
----------
 * If your modpack is distributed via a big name launcer(FTB, ATLauncer, etc.) and has separate  client and server downloads. You must contact me to get permission to include ESM.

Mod Developers
--------
An API is provided to register your commands with EnhancedServerModeration. It is simple, have your command extend EsmCommandBase and implement IEsmCommand then register your command in your FMLServerStartingEvent like with EsmRegisterCommand.registerCommand(event, new YourCommand());


Two new methods are usable by your command's class.

 * Override these to set your own id and description.
* getCommandId() returns a String with the id of your command. It what is used when an admin gives the command to a player. Defaults to your getCommandName()
* getCommandDescription() returns a string with a small description of your command. This is used when the admin uses the /printcommands feature. It creates a list of commands with their corresponding description. defaults to your getCommandUsage()

 - Sends UniqueWorldID packet on channel - "world_info" 
 
License
----
    EnhancedServerModeration is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    EnhancedServerModeration is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with EnhancedServerModeration.  If not, see <http://www.gnu.org/licenses/>.

[Twitter]:https://twitter.com/mysticdrew_
[MinecraftForum]:http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/wip-mods/2222334-enhancedservermoderation-0-5-3
[Curse]:http://minecraft.curseforge.com/mc-mods/224460-enhanced-server-moderation
[FTB]:http://forum.feed-the-beast.com/threads/1-7-10-enhancedservermoderation.51592/